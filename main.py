import webapp2
import urllib2
from apiclient import discovery
from httplib2 import Http
import logging
import json
from oauth2client.contrib.appengine import AppAssertionCredentials



weather_Api = "9436db13deb052198685e5748b5e5d59"




# 1. Create a handler
class MainHandler(webapp2.RequestHandler):

    def get(self):

        bq = discovery.build('bigquery', 'v2', http=Http(timeout=30))

        weather = urllib2.urlopen("https://api.openweathermap.org/data/2.5/weather?q={}&APPID={}".format("portland",weather_Api))
        weather = weather.read()
        weather = json.loads(weather)

        http_auth = None
        if http_auth is None:
            credentials = AppAssertionCredentials('https://www.googleapis.com/auth/bigquery')
            http_auth = credentials.authorize(Http(timeout=30))

        insertAllBody = {
            "skipInvalidRows": False,
            "ignoreUnknownValues": False,
            "rows": [
                {
                    "json": weather
                }
            ]
        }

        req = bq.tabledata().insertAll(projectId="bootcamp-ben", datasetId="weather", tableId="portland",
                                       body=insertAllBody)
        logging.info(insertAllBody)
        response = req.execute(http=http_auth)
        logging.info(response)
        self.response.write(response)




# 2. Add it to the routing
app = webapp2.WSGIApplication([
    ('/weather', MainHandler)
], debug=True)