import webapp2
import urllib2
from apiclient import discovery
from httplib2 import Http
import logging
import json
from oauth2client.contrib.appengine import AppAssertionCredentials
from datetime import  datetime, timedelta
import time


# 1. Create a handler
class JobsHandler(webapp2.RequestHandler):

    def get(self):

        prev_24_hours = datetime.now() - timedelta(hours=24)
        prev_24_hours = prev_24_hours.strftime('%s')


        project_id = "bootcamp-ben"
        bq = discovery.build('bigquery', 'v2', http=Http(timeout=30))
        jobs = bq.jobs()
        http_auth = None
        if http_auth is None:
            credentials = AppAssertionCredentials(['https://www.googleapis.com/auth/bigquery','https://www.googleapis.com/auth/cloud-platform'])
            http_auth = credentials.authorize(Http(timeout=30))
        jobs = jobs.list(projectId=project_id,allUsers=True,minCreationTime=prev_24_hours).execute(http=http_auth)
        jobs = jobs.get("jobs")
        each_job_id = []
        for job in jobs:
            job = json.dumps(job)
            job = json.loads(job)

            insertAllBody = {
                "skipInvalidRows": False,
                "ignoreUnknownValues": False,
                "rows": [
                    {
                        "json": job
                    }
                ]
            }

            req = bq.tabledata().insertAll(projectId="bootcamp-ben", datasetId="weather", tableId="jobs",
                                           body=insertAllBody)

            logging.info(insertAllBody)
            response = req.execute(http=http_auth)
            logging.info(response)
            self.response.write(response)




# 2. Add it to the routing
app = webapp2.WSGIApplication([
    ('/jobs', JobsHandler)
], debug=True)