#Bootcamp Project

## Code Style Guidelines
Should roughly follow standard Python conventions described here:
https://www.python.org/dev/peps/pep-0008/

Main things that should be followed without exception:

- class names should use CapWords convention
- functions and variable names should be lowercase with words separated by underscores if necessary to improve readability
- class method names and instance variables should be lowercase with words separated by underscores if necessary to improve readability
- constats should be all in caps with words separated by underscores
- private variables and methods should start with _

Here in more detail: https://bitbucket.org/analyticspros/ap-labs/src/master/python_code_style_guidelines.md

## Project Setup
Project uses Python 2.7.

If you need a fresh install of libs, move to project's root folder and run:
```
pip install -t lib -r requirements.txt
```
This will install all Python packages to a lib folder in your
root project directory.

## Dependency Injection
```
di.py
```
We add all our servies here, so they can be easily injected at
various points in the app.

https://pypi.org/project/Inject/

This makes it easy to prepare service classes or configuration for different environments like development and production.
You can request those classes from anywhere within your code most commonly as part of the constructor:
```python
@container.params(wc=IWeatherClient, bq="bq", bqcfg="bqcfg")
def __init__(self, request=None, response=None,
             wc=None, bq=None, bqcfg=None):
    super(WeatherStreamingTaskHandler, self).__init__(request, response)
    self.wc = wc
    assert isinstance(wc, IWeatherClient)

    self.bq = bq
    assert isinstance(bq, BigQueryService)

    self.bqcfg = bqcfg
```
or within block of code:
```python
self.bq = container.instance("bq")
```

## Data
Data is stored here: https://bigquery.cloud.google.com/dataset/analyticspros.com:spotted-cinnamon-834:bootcamp?pli=1

Tables prefixed with ut_ are test tables and should not be used for visualization.

### weather table
The requirement is to stream weather data every hour into BigQuery.
We have to consider what table partitioning type makes the most sense. Since the data we are adding will be added
continously every hour it would be hard to partition in with sharded tables (each day a new date prefixed table).
We'd have to make sure we create a new table each new day first before streaming into it.

So a much easier appraoch is to use a single table to stream to all the time.
Because we have timestamp data inside our schema, we can use column-based partitioning on the
timestamp column.

Additionally we can use clustering on name and country columns as they are most likely columns
to be queried in the WHERE clause to filter data.

### bqjobs_YYYYMMDD table
The requirement is to load yesterday's BQ job data into BigQuery.
Here we have more option regarding paritioning and any BQ partitioning type would work.

In this case we used day sharded partitioning, because it's quite easy to QA if daily data is flowing
into BQ, you just need to look at the last created table.
As well as, we have a complete set of yesterday's data so it's easy to just load it
into a separate table containing that date's data.

Another bonus is when you want to change something with the tables,
you can easily run a backfill just overwriting the already existing tables.

## Unit tests
All unit tests are in a unittests folder.
Running ```test.py``` will run all tests in a bundle. This is nice for continous integration so you can run all tests before deploying.

Unit tests are independent of GAE SDK and run using a normal python env without the need
of running anyhing through GAE dev server.